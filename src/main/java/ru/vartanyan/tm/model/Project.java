package ru.vartanyan.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.entity.IWBS;

@Setter
@Getter
@NoArgsConstructor
public class Project extends AbstractBusinessEntity implements IWBS {

    public Project(@NotNull final String name,
                   @Nullable final String description) {
        this.name = name;
        this.description = description;
    }

}
