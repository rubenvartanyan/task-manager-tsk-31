package ru.vartanyan.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.enumerated.Status;

import java.util.Date;


@Getter
@Setter
@NoArgsConstructor
public class AbstractBusinessEntity extends AbstractEntity{

    @NotNull public String name = "";

    @NotNull public String description = "";

    @NotNull public String userId;

    @NotNull public Status status = Status.NOT_STARTED;

    @Nullable public Date dateStarted;

    @Nullable public Date dateFinish;

    @NotNull public Date created = new Date();

    @Override
    public String toString() {
        return getId() + ": " + name + "; " + description + "; " + "User Id: " + getUserId();
    }

}
