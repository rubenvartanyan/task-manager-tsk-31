package ru.vartanyan.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class Command {

    @NotNull public String name = "";

    @NotNull public String arg = "";

    @NotNull public String description = "";

    public Command(@NotNull final String name,
                   @NotNull final String arg,
                   @NotNull final String description) {
        this.name = name;
        this.arg = arg;
        this.description = description;
    }

    @Override
    public String toString() {
        @NotNull String result = "";
        if (!name.isEmpty()) result += name;
        if (!arg.isEmpty()) result += " [" + arg + "] ";
        if (!description.isEmpty()) result += " - " + description;
        return result;
    }

}

