package ru.vartanyan.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.entity.IWBS;

@Getter
@Setter
@NoArgsConstructor
public class Task extends AbstractBusinessEntity implements IWBS {

    @Nullable private String projectId;

    public @Nullable String getProjectId() {
        return projectId;
    }

    public void setProjectId(@Nullable final String projectId) {
        this.projectId = projectId;
    }

}
