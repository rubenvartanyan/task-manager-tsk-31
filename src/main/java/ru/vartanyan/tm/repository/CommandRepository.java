package ru.vartanyan.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.repository.ICommandRepository;
import ru.vartanyan.tm.command.AbstractCommand;
import ru.vartanyan.tm.model.Command;

import java.util.*;
import java.util.stream.Collectors;

public class CommandRepository implements ICommandRepository {

    @Nullable private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Nullable private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    @NotNull
    @Override
    public Collection<AbstractCommand> getCommand() {
        return commands.values();
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @NotNull
    @Override
    public final Collection<String> getCommandNames() {
        @Nullable final List<String> result = new ArrayList<>();
        for (@Nullable final AbstractCommand command: commands.values()) {
            @Nullable final String name = command.name();
            if (name.isEmpty()) continue;
            result.add(name);
        }
        return result;
    }

    @NotNull
    @Override
    public final Collection<AbstractCommand> getArgsCommand() {
        @Nullable final List<AbstractCommand> result = new ArrayList<>();
        for (@Nullable final AbstractCommand command: commands.values()) {
            @Nullable final String argument = command.arg();
            if (argument == null || argument.isEmpty()) continue;
            result.add(command);
        }
        return result;
    }

    @NotNull
    @Override
    public final Collection<String> getCommandsArgs() {
        @Nullable final List<String> result = new ArrayList<>();
        for (@Nullable final AbstractCommand command: commands.values()) {
            @Nullable final String argument = command.arg();
            if (argument == null || argument.isEmpty()) continue;
            result.add(argument);
        }
        return result;
    }

    @Nullable
    @Override
    public final AbstractCommand getCommandByName(@NotNull final String name) {
        return commands.get(name);
    }

    @Nullable
    @Override
    public final AbstractCommand getCommandByArg(@NotNull final String arg) {
        return commands.get(arg);
    }

    @Override
    public void add(@NotNull final AbstractCommand command) {
        @Nullable final String arg = command.arg();
        @Nullable final String name = command.name();
        if (arg != null) arguments.put(arg, command);
        commands.put(name, command);
    }

    @Override
    public List<String> getCommandList() {
        return commands.values().stream()
                .map(AbstractCommand::name)
                .filter(name -> !name.isEmpty())
                .collect(Collectors.toList());
    }

    @Nullable
    private final List<AbstractCommand> commandList = new ArrayList<>();

}
