package ru.vartanyan.tm.bootstrap;


import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.repository.ICommandRepository;
import ru.vartanyan.tm.api.repository.IProjectRepository;
import ru.vartanyan.tm.api.repository.ITaskRepository;
import ru.vartanyan.tm.api.repository.IUserRepository;
import ru.vartanyan.tm.api.service.*;
import ru.vartanyan.tm.command.AbstractCommand;
import ru.vartanyan.tm.component.Backup;
import ru.vartanyan.tm.component.FileScanner;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.repository.*;
import ru.vartanyan.tm.service.*;
import ru.vartanyan.tm.util.SystemUtil;
import ru.vartanyan.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;


@Getter
public final class Bootstrap implements ServiceLocator{

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();
    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private Backup backup = new Backup(this);

    @NotNull
    private FileScanner fileScanner = new FileScanner(this);

    void initUser() throws Exception {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    @SneakyThrows
    public void initPID() {
        final String fileName = "task-manager.pid";
        final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes(StandardCharsets.UTF_8));
        final File file = new File(fileName);
        file.deleteOnExit();
    }

    @SneakyThrows
    private void initCommand() throws Exception {
        @NotNull final Reflections reflections = new Reflections("ru.vartanyan.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(ru.vartanyan.tm.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            registry(clazz.newInstance());
        }
    }

    public void run(@NotNull final String... args) throws Throwable {
        loggerService.debug("TEST!");
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        initPID();
        initCommand();
        initUser();
        backup.init();
        fileScanner.init();
        while (true){
            System.out.println();
            System.out.println("ENTER COMMAND:");
            @NotNull final String command = TerminalUtil.nextLine();
            loggerService.command(command);
            try {
                parseCommand(command);
                System.out.println("[OK]");
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    public boolean parseArgs(@Nullable String[] args) throws Exception {
        if (args == null || args.length == 0) return false;
        @NotNull final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private void parseArg(@Nullable final String arg) throws Exception {
        if (arg == null || arg.isEmpty()) return;
        @NotNull final AbstractCommand command = commandService.getCommandByName(arg);
        if (command == null) showIncorrectArg();
        else command.execute();
    }

    public void showIncorrectArg() {
        System.out.println("Error! Argument not found...");
    }

    public void parseCommand(@Nullable final String cmd) throws Exception {
        if (cmd == null || cmd.isEmpty()) return;
        @Nullable final AbstractCommand command = commandService.getCommandByName(cmd);
        if (command == null) {
            showIncorrectCommand();
            return;
        }
        @NotNull final Role[] roles = command.roles();
        authService.checkRoles(roles);
        command.execute();
    }

    private void registry(@Nullable final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public static void showIncorrectCommand() {
        System.out.println("Error! Command not found...");
    }

}
