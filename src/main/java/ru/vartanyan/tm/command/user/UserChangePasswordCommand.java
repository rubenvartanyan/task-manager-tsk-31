package ru.vartanyan.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.command.AbstractUserCommand;
import ru.vartanyan.tm.util.TerminalUtil;

public class UserChangePasswordCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "user-change-password";
    }

    @Override
    public String description() {
        return "Change user password";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CHANGE PASSWORD]");
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER NEW PASSWORD]");
        @NotNull final String newPassword = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(userId, newPassword);
    }

}
