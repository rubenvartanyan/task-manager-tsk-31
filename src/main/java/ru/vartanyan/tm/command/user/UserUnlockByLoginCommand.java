package ru.vartanyan.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.command.AbstractAuthCommand;
import ru.vartanyan.tm.command.AbstractUserCommand;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.util.TerminalUtil;

public class UserUnlockByLoginCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "user-unlock-by-login";
    }

    @Override
    public String description() {
        return "Unlock user by login";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UNLOCK USER BY LOGIN]");
        System.out.println("[ENTER LOGIN]");
        @NotNull final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().unlockUserByLogin(login);
    }

    @Override
    public Role[] roles() {
        return new Role[] {
                Role.ADMIN
        };
    }

}
