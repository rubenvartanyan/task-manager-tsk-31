package ru.vartanyan.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.command.AbstractProjectCommand;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.util.TerminalUtil;

public class ProjectStartByNameCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-init-by-name";
    }

    @Override
    public String description() {
        return "Start project by name";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[START PROJECT]");
        if (serviceLocator.getAuthService().isNotAuth()) throw new NotLoggedInException();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[ENTER NAME]");
        @NotNull final String name = TerminalUtil.nextLine();
        serviceLocator.getProjectService().startEntityByName(name, userId);
        System.out.println("[PROJECT STARTED]");
    }

}
