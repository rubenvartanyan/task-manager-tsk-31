package ru.vartanyan.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.command.AbstractTaskCommand;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.model.Task;
import ru.vartanyan.tm.util.TerminalUtil;

public class TaskStartByIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "task-init-by-id";
    }

    @Override
    public String description() {
        return "Start task by id";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator.getAuthService().isNotAuth()) throw new NotLoggedInException();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[START TASK]");
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        serviceLocator.getTaskService().startEntityById(id, userId);
        System.out.println("[TASK STARTED]");
    }

}
