package ru.vartanyan.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.command.AbstractCommand;
import ru.vartanyan.tm.dto.Domain;
import ru.vartanyan.tm.exception.system.NotLoggedInException;

import java.util.Objects;

public abstract class AbstractDataCommand extends AbstractCommand {

    public static final String BACKUP_LOAD = "backup-load";
    public static final String BACKUP_SAVE = "backup-save";

    protected static final String FILE_BINARY = "./data.bin";
    protected static final String FILE_BASE64 = "./data.base64";

    protected static final String FILE_FASTERXML_JSON = "./data-fasterxml.json";
    protected static final String FILE_FASTERXML_XML = "./data-fasterxml.xml";
    protected static final String FILE_FASTERXML_YAML = "./data-fasterxml.yaml";

    protected static final String FILE_JAXB_JSON = "./data-jaxb.json";
    protected static final String FILE_JAXB_XML = "./data-jaxb.xml";
    protected static final String BACKUP_XML = "./backup.xml";

    protected static final String JAVAX_XML_PROPERTY = "javax.xml.bind.context.factory";
    protected static final String ORG_ECLIPSE_PROPERTY = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    protected static final String ECLIPSELINK_PROPERTY = "eclipselink.media-type";
    protected static final String APPLICATION_JSON_PROPERTY = "application/json";

    @NotNull
    @SneakyThrows
    public Domain getDomain() throws Exception {
        final Domain domain = new Domain();
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    @SneakyThrows
    public void setDomain(final Domain domain) {
        if (domain == null) return;
        serviceLocator.getProjectService().clear();
        serviceLocator.getProjectService().addAll(domain.getProjects());
        serviceLocator.getTaskService().clear();
        serviceLocator.getTaskService().addAll(domain.getTasks());
        serviceLocator.getUserService().clear();
        serviceLocator.getUserService().addAll(domain.getUsers());
        serviceLocator.getAuthService().logout();
    }

}
