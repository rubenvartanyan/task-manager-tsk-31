package ru.vartanyan.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.dto.Domain;
import ru.vartanyan.tm.enumerated.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DataYamlLoadFasterXmlCommand extends AbstractDataCommand{

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-yaml-fasterxml-load";
    }

    @Nullable
    @Override
    public  String description() {
        return "Load data from YAML fasterxml";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA YAML FASTERXML LOAD]");
        final String json = new String(Files.readAllBytes(Paths.get(FILE_FASTERXML_YAML)));
        final ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
        final Domain domain = objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

    @NotNull
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
