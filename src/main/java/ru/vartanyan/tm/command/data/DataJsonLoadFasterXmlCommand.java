package ru.vartanyan.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.dto.Domain;
import ru.vartanyan.tm.enumerated.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DataJsonLoadFasterXmlCommand extends AbstractDataCommand{

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-json-fasterxml-load";
    }

    @Nullable
    @Override
    public String description() {
        return "Load data from JSON fasterxml";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON FASTERXML LOAD]");
        final String json = new String(Files.readAllBytes(Paths.get(FILE_FASTERXML_JSON)));
        final ObjectMapper objectMapper = new ObjectMapper();
        final Domain domain = objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

    @NotNull
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
