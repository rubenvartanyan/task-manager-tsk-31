package ru.vartanyan.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.dto.Domain;
import ru.vartanyan.tm.enumerated.Role;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class DataBinLoadCommand extends AbstractDataCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "data-bin-load";
    }

    @Override
    public @Nullable String description() {
        return "Load bin data";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BIN LOAD]");
        @NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_BINARY);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
        objectInputStream.close();
        fileInputStream.close();
    }

    @NotNull
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
