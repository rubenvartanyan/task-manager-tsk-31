package ru.vartanyan.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.model.AbstractEntity;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void add(@NotNull final E entity);

    void remove(@NotNull final E entity);

    @Nullable
    E findById(@NotNull final String id) throws Exception;

    void removeById(@NotNull final String id) throws Exception;

    @NotNull
    List<E> findAll();

    void clear();

    void addAll(final List<E> entities);

}
