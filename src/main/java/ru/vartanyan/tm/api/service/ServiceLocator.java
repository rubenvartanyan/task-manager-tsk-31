package ru.vartanyan.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.api.IPropertyService;

public interface ServiceLocator {

    @NotNull
    IUserService getUserService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IPropertyService getPropertyService();

}
