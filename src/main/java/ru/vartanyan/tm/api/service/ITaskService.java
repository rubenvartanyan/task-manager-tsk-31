package ru.vartanyan.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.api.IBusinessService;
import ru.vartanyan.tm.model.Task;

public interface ITaskService extends IBusinessService<Task> {

    void add(@NotNull final String name,
             @NotNull final String description,
             @NotNull final String userId) throws Exception;

}
