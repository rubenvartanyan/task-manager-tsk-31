package ru.vartanyan.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.model.User;

public interface IAuthService {

    @Nullable
    User getUser() throws Exception;

    @Nullable
    String getUserId() throws Exception;

    boolean isNotAuth();

    void logout();

    void login(@NotNull final String login,
               @NotNull final String password) throws Exception;

    void registry(@NotNull final String login,
                  @NotNull final String password,
                  @NotNull final String email) throws Exception;

    void checkRoles(@NotNull final Role... roles) throws Exception;

}
