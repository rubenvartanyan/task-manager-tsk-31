package ru.vartanyan.tm.api.other;

import org.jetbrains.annotations.NotNull;

public interface ISaltSetting {

    @NotNull
    String getPasswordSecret();

    @NotNull
    Integer getPasswordIteration();

    @NotNull
    String getDeveloperName();

    @NotNull
    String getDeveloperEmail();

}
