package ru.vartanyan.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.model.AbstractEntity;

import java.util.List;

public interface IService<E extends AbstractEntity> extends IRepository<E> {
    @NotNull List<E> findAll();
}
