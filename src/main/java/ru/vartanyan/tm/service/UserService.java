package ru.vartanyan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.repository.IUserRepository;
import ru.vartanyan.tm.api.service.IUserService;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.exception.empty.*;
import ru.vartanyan.tm.model.User;
import ru.vartanyan.tm.util.HashUtil;

import java.util.List;
import java.util.Objects;

public class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final IPropertyService propertyService;

    public UserService(@NotNull final IUserRepository userRepository,
                       @NotNull final IPropertyService propertyService) {
        super(userRepository);
        this.userRepository = userRepository;
        this.propertyService = propertyService;
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) throws Exception {
        if (login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public void removeByLogin(@NotNull final String login) throws Exception {
        if (login.isEmpty()) throw new EmptyLoginException();
        userRepository.removeByLogin(login);
    }

    @NotNull
    @Override
    public User create(@NotNull final String login,
                       @NotNull final String password) throws Exception {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(Objects.requireNonNull(HashUtil.salt(propertyService, password)));
        user.setRole(Role.USER);
        userRepository.add(user);
        return user;
    }

    @Override
    public void create(@NotNull final String login,
                       @NotNull final String password,
                       @NotNull final String email) throws Exception {
        if (email.isEmpty()) throw new EmptyEmailException();
        @NotNull final User user = create(login, password);
        user.setEmail(email);
    }

    @Override
    public void create(@NotNull String login,
                       @NotNull String password,
                       @Nullable Role role) throws Exception {
        if (role == null) throw new EmptyRoleException();
        @NotNull final User user = create(login, password);
        user.setRole(role);
    }

    @Override
    public void setPassword(@NotNull final String userId,
                            @NotNull final String password) throws Exception {
        if (userId.isEmpty()) throw new EmptyIdException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = findById(userId);
        if (user == null) return;
        @NotNull final String hash = Objects.requireNonNull(HashUtil.salt(propertyService, password));
        user.setPasswordHash(hash);
    }

    @Override
    public void updateUser(@NotNull final String userId,
                           final @Nullable String firstName,
                           final @Nullable String lastName,
                           final @Nullable String middleName
                           ) throws Exception {
        @Nullable final User user = findById(userId);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
    }

    @Override
    public void unlockUserByLogin(@NotNull String login) throws Exception {
        if (login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) return;
        user.setLocked(false);
    }

    @Override
    public void unlockUserById(@NotNull String id) throws Exception {
        if (id.isEmpty()) throw new EmptyIdException();
        @Nullable final User user = userRepository.findById(id);
        if (user == null) return;
        user.setLocked(false);
    }

    @Override
    public void lockUserByLogin(@NotNull String login) throws Exception {
        if (login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) return;
        user.setLocked(true);
    }

    @Override
    public void lockUserById(@NotNull String id) throws Exception {
        if (id.isEmpty()) throw new EmptyIdException();
        @Nullable final User user = userRepository.findById(id);
        if (user == null) return;
        user.setLocked(true);
    }

    public @NotNull List<User> findAll() {
        return userRepository.findAll();
    }

    public void clear() {
        userRepository.clear();
    }

}
